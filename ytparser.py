#!/usr/bin/python3

import urllib.request
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys

class YTHandler(ContentHandler):
    def __init__(self):
        super().__init__()
        self.in_entry = False
        self.in_content = False
        self.content = ""
        self.title = ""
        self.link = ""
        self.videos = []
    def startElement(self, name, attrs):
        if name == "entry":
            self.in_entry = True
        elif self.in_entry:
            if name == "title":
                self.in_content = True
            elif name == "link":
                self.in_content = True
            elif name == "link":
                self.link = attrs.get("href")

    def endElement(self, name):
        if name == "entry":
            self.in_entry = False
            self.videos.append((self.link, self.title))
        elif self.in_entry:
            if name == "title":
                self.tile = self.content
                self.content = ""
                self.in_content = False

    def characters(self, chars):
        if self.in_content:
            self.content += chars

    def get_videos_html(self):
        return "\n".join(f" <li><a href='{link}'>{title}</a></li>" for link, title in self.videos)

def main(channel_id):
    parser = make_parser()
    handler = YTHandler()
    parser.setContentHandler(handler)

    url_xmlfile = f"https://www.youtube.com/feeds/videos.xml?channel_id={channel_id}"
    parser.parse(urllib.request.urlopen(url_xmlfile))

    videos_html = handler.get_videos_html()

    page = f"""
        <!DOCTYPE html>
        <html lang="en">
            <body>
                <h1>Channel contents:</h1>
                <ul>
                {videos_html}
                </ul>
            </body>
        </html>
    """
    print(page)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python xml-parser-youtube.py <id_canal>")
        print()
        print(" <id_canal>: channel id of the channel for downloading")
        sys.exit(1)
    channel_id = sys.argv[1]
    main(channel_id)
